# for installation, can be overriden on command line
PREFIX = /usr/local

# get directory containing the Makefile, based on
# http://stackoverflow.com/a/18137056/5624984
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(patsubst %/,%,$(dir $(mkfile_path)))

.PHONY: install uninstall test

install:
	[ -d $(PREFIX)/bin ] || install -d -m 755 $(PREFIX)/bin
	install -m 755 git-import-hgs.sh $(PREFIX)/bin/git-import-hgs

uninstall:
	rm -f $(PREFIX)/bin/git-import-hgs

test:
	docker run -it --rm --volume=$(mkfile_dir):/src --workdir=/src \
	  dduportal/bats test/
