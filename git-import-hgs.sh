#!/bin/bash

rsync_options=(
  --human-readable --stats --itemize-changes
  --del --partial-dir=.rsync-tmp
  --recursive --links --times
  --executability
  --cvs-exclude
  --exclude='*.swp'

#   --list-only
#   --dry-run
)

# RE:^(?!(?:etc|ftp|lib|include|menu|objects|prog|rep|script|valid)(?:/|$)).*
# !.bzrignore
bzrignore_filters=(
  # bzrignore: *.r
  --filter 'exclude *.r'
  # bzrignore: .dbg
  --filter 'exclude **/.dbg/'
  # bzrignore: .pct
  --filter 'exclude **/.pct/'
  # bzrignore: prog/support/**/*
  --filter 'exclude /prog/support/'
  # bzrignore: RE:^etc(/|$).*
  --filter 'include /etc/***'
  # bzrignore: RE:^ftp(/|$).*
  --filter 'include /ftp/***'
  # bzrignore: RE:^gradle(/|$).*
  --filter 'include /gradle/***'
  # bzrignore: RE:^lib(/|$).*
  --filter 'include /lib/***'
  # bzrignore: RE:^include(/|$).*
  --filter 'include /include/***'
  # bzrignore: RE:^menu(/|$).*
  --filter 'include /menu/***'
  # bzrignore: RE:^objects(/|$).*
  --filter 'include /objects/***'
  # bzrignore: RE:^prog(/|$).*
  --filter 'include /prog/***'
  # bzrignore: RE:^rep(/|$).*
  --filter 'include /rep/***'
  # bzrignore: RE:^script(/|$).*
  --filter 'include /script/***'
  # bzrignore: RE:^valid(/|$).*
  --filter 'include /valid/***'
  # bzrignore: ![a-z]*.gradle
  --filter 'include [a-z]*.gradle'
  # bzrignore: !gradlew*
  --filter 'include gradlew*'
  # bzrignore: <implied by first RE: rule, anything not matched earlier>
  --filter 'exclude *'
)

import_hwl_filters=(
)


function url2rsync() { # LOCATION
  local location scheme

  location="${1:?specify location to import from (ssh:// URL)}"
  scheme="${location%%://*}"

  if [[ ${scheme} == ${location} ]]; then
    echo "not a valid URL: ${location}" >&2
    return 1
  fi

  case "${scheme}" in
    rsync) printf "${location}"; return ;;
    ssh|ssh+rsync)
      local rs_loc rs_host rs_path
      rs_loc="${location#${scheme}://}"
      rs_host="${rs_loc%%/*}"

      if [[ ${rs_host} == ${rs_loc} ]]; then
        echo "URL contains no path component: ${location}" >&2
        return 1
      fi

      if [[ ${rs_host/:/} != ${rs_host} ]]; then
        # URL includes a port number, print rsync connection params
        # required for this
        local rs_port
        rs_port="${rs_host##*:}"
        rs_host="${rs_host%:*}"

        printf '%s\n' --rsh "ssh -p ${rs_port}"
      fi

      rs_path="/${rs_loc#*/}"
      printf '%s:%s' "${rs_host}" "${rs_path}"
      ;;
    *) echo "protocol '${scheme}' not supported" >&2; return 1 ;;
  esac
}

function do_import() { # [RSYNC_ARGS] SOURCE
  if [[ -z ${1:-} ]]; then
    echo "specify SOURCE to import from" >&2
    return 2
  fi

  rsync "${rsync_options[@]}" "${bzrignore_filters[@]}" "$@" ./
}

function main() { # [RSYNC_ARGS] LOCATION
  if [[ -z ${1:-} ]]; then
    echo "Usage: $0 [RSYNC_ARGS] LOCATION" >&2
    exit 2
  fi

  local -a rsync_args
  # initialise with all arguments except the last (LOCATION)
  rsync_args=( "${@:1:$# - 1}" )

  # add arguments returned by url2rsync
  oIFS="${IFS}"
  IFS=$'\n'  # url2rsync returns newline-separated args
  rsync_args+=( $(url2rsync "${@: -1}") )
  IFS="${oIFS}"

  # ensure SOURCE path ends with '/' as we want to import the
  # *contents* into source control working tree
  rsync_args[-1]="${rsync_args[-1]%/}/"

  do_import "${rsync_args[@]}"
}


# if not being sourced
if [[ $0 == ${BASH_SOURCE[0]} ]]; then
  set -e
  main "$@"
fi
