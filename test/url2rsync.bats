#!/usr/bin/env bats

load "${BATS_TEST_DIRNAME}/common.bash"

source "${src_dir}/git-import-hgs.sh"


@test "url2rsync: accepts SSH URL" {
  run url2rsync ssh://somehost.domain.tld/path/to/src/

  (( status == 0 ))
  [[ ${output} == 'somehost.domain.tld:/path/to/src/' ]]
}

@test "url2rsync: accepts SSH+rsync URL" {
  run url2rsync ssh+rsync://somehost.domain.tld/path/to/src/

  (( status == 0 ))
  [[ ${output} == 'somehost.domain.tld:/path/to/src/' ]]
}

@test "url2rsync: accepts rsync URL" {
  run url2rsync rsync://somehost.domain.tld/path/to/src/

  (( status == 0 ))
  [[ ${output} == 'rsync://somehost.domain.tld/path/to/src/' ]]
}

@test "url2rsync: accepts short hostname in URL" {
  run url2rsync ssh://somehost/path/to/src/

  (( status == 0 ))
  [[ ${output} == 'somehost:/path/to/src/' ]]
}

@test "url2rsync: accepts port in URL" {
  run url2rsync ssh://somehost.domain.tld:2222/path/to/src/

  (( status == 0 ))
  [[ ${lines[0]} == '--rsh' ]]
  [[ ${lines[1]} == 'ssh -p 2222' ]]
  [[ ${lines[2]} == 'somehost.domain.tld:/path/to/src/' ]]
}

@test "url2rsync: rejects other URL schemes" {
  run url2rsync sftp://somehost.domain.tld/path/to/src/

  (( status == 1 ))
  [[ ${output} == "protocol 'sftp' not supported"* ]]
}

@test "url2rsync: rejects URLs without a path" {
  run url2rsync ssh://somehost.domain.tld

  (( status == 1 ))
  [[ ${output} == "URL contains no path component: ssh://somehost."* ]]
}

@test "url2rsync: rejects URLs without a scheme" {
  run url2rsync somehost.domain.tld:/path/to/src/

  (( status == 1 ))
  [[ ${output} == "not a valid URL: somehost.domain.tld"* ]]
}

@test "url2rsync: fails when no URL provided" {
  run url2rsync

  (( status == 1 ))
  [[ ${output} == *"specify location to import from"* ]]
}
