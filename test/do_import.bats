#!/usr/bin/env bats

load "${BATS_TEST_DIRNAME}/common.bash"

source "${src_dir}/git-import-hgs.sh"

setup() {
  path_add "${mock_dir}"
}

teardown() {
  path_rm "${mock_dir}"

  debug_out
}


@test "do_import: it runs rsync with specified args" {
  run do_import --foobared baz:/baz

  (( status == 0 ))
  [[ ${output} == *'"--foobared"'* ]]
  [[ ${output} == *'"baz:/baz"'* ]]
}

@test "do_import: it fails when no args specified" {
  run do_import

  (( status == 2 ))
  [[ ${output} == *"specify SOURCE to import from"* ]]
}
